package main.java.planificarezboruri.datahandler;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import main.java.planificarezboruri.beans.InformatiiZboruri;
import main.java.planificarezboruri.graf.Arc;
import main.java.planificarezboruri.graf.Eticheta;
import main.java.planificarezboruri.graf.Graf;
import main.java.planificarezboruri.graf.Nod;

public class DataHandler {

	public static InformatiiZboruri read() {
		int numarZboruri = 0, numarEchipaje = 0;
		boolean[][] planificare = null;
		Scanner scanner = null;
		File file = new File(
				"C:\\Users\\HrihorRa\\Desktop\\Facultate\\AnulII\\SemestrulII\\Automate Calculabilitate si Complexitate\\acc-planificare-zboruri\\Planificare Zboruri\\src\\main\\resources\\input.txt");

		try {
			scanner = new Scanner(file);
			numarEchipaje = scanner.nextInt();
			numarZboruri = scanner.nextInt();
			planificare = new boolean[numarZboruri][numarEchipaje];

			for (int i = 0; i < numarZboruri; i++) {
				for (int j = 0; j < numarEchipaje; j++) {
					if (scanner.hasNext()) {
						planificare[i][j] = scanner.nextInt() == 0 ? false : true;
					}
				}
			}
			scanner.close();
			InformatiiZboruri informatii = new InformatiiZboruri(numarZboruri, numarEchipaje, planificare);
			return informatii;
		} catch (Exception e) {
			System.out.println("Eroare la citirea din fisier !" + e);
			return null;
		}
	}

	public static void print(Graf graf) {
		List<Integer> result = new ArrayList<>();
		for (String key : graf.getNoduri().keySet()) {
			Nod nod = graf.getNoduri().get(key);
			if (nod.getEticheta().equals(Eticheta.AERONAVA)) {
				boolean planned = false;
				for (Nod end : nod.getListaAdiacenta()) {
					if (graf.getArce().get(String.valueOf(nod.getNumber()).concat(String.valueOf(end.getNumber())))
							.getFlux() == 1) {
						planned = true;
						result.add(end.getValoare());
						break;
					}
				}
				if (!planned) {
					result.add(-1);
				}
			}
		}

		try {
			File file = new File(
					"C:\\Users\\HrihorRa\\Desktop\\Facultate\\AnulII\\SemestrulII\\Automate Calculabilitate si Complexitate\\acc-planificare-zboruri\\Planificare Zboruri\\src\\main\\resources\\output.txt");
			PrintStream writer = new PrintStream(file);
			for (Integer value : result) {
				writer.print(value + " ");
			}
			writer.println();
			if (result.contains(-1)) {
				writer.println("NU S-AU PUTUT EFECTUA TOATE ZBOURILE!");
			} else {
				writer.println("ZBORURILE AU FOST PLANIFICATE CU SUCCES!");
			}
			writer.close();
		} catch (Exception e) {
			System.out.println("Eroare la scrierea in fisier !" + e);
		}

	}

}
