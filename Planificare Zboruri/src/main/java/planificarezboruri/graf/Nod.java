package main.java.planificarezboruri.graf;

import java.util.ArrayList;
import java.util.List;

public class Nod {
	private Eticheta eticheta;
	private Integer valoare;
	private Integer number;
	private List<Nod> listaAdiacenta;
	
	public Eticheta getEticheta() {
		return eticheta;
	}
	public void setEticheta(Eticheta eticheta) {
		this.eticheta = eticheta;
	}
	public Integer getValoare() {
		return valoare;
	}
	public void setValoare(Integer valoare) {
		this.valoare = valoare;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public List<Nod> getListaAdiacenta() {
		return listaAdiacenta;
	}
	public void setListaAdiacenta(List<Nod> listaAdiacenta) {
		this.listaAdiacenta = listaAdiacenta;
	}
	
	public void adaugaInListaDeAdiacenta(Nod nod) {
		listaAdiacenta.add(nod);
	}
	
	public Nod() {
		
	}
	
	public void removeNodeFromAdiacentList(String nodNumber) {
		int index =0;
		for(Nod nod: listaAdiacenta) {
			if(nod.getNumber().toString().equals(nodNumber)) {
				break;
			}
			index++;
		}
		listaAdiacenta.remove(index);
	}
	
	public Nod(Nod that) {
		this(that.getEticheta(), that.getValoare(), that.getNumber());
	}
	
	public Nod(Eticheta eticheta, Integer valoare, Integer number) {
		this.eticheta = eticheta;
		this.valoare = valoare;
		this.listaAdiacenta = new ArrayList<>();
		this.number = number;
	}
	
	
	
}
