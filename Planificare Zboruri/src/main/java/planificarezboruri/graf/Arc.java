package main.java.planificarezboruri.graf;

public class Arc {
	
	private static final int FLUX_INITIAL = 0;
	private static final int CAPACITATE = 1;
	
	private Nod start;
	private Nod end;
	private int flux;
	private int capacitate;
	private int capacitateReziduala;

	
	public Nod getStart() {
		return start;
	}
	public void setStart(Nod start) {
		this.start = start;
	}
	public Nod getEnd() {
		return end;
	}
	public void setEnd(Nod end) {
		this.end = end;
	}
	public int getCapacitateReziduala() {
		return capacitateReziduala;
	}
	public void setCapacitateReziduala(int capacitateReziduala) {
		this.capacitateReziduala = capacitateReziduala;
	}
	public int getFlux() {
		return flux;
	}
	public void setFlux(int flux) {
		this.flux = flux;
	}
	public int getCapacitate() {
		return capacitate;
	}
	public void setCapacitate(int capacitate) {
		this.capacitate = capacitate;
	}

	public Arc invers() {
		Arc arcInvers = new Arc();
		arcInvers.setCapacitate(capacitate);
		arcInvers.setCapacitateReziduala(capacitateReziduala);
		arcInvers.setFlux(flux);
		arcInvers.setEnd(new Nod(start));
		arcInvers.setStart(new Nod(end));
		return arcInvers;
	}
	
	public Arc() {

	}
	
	public Arc(Arc that) {
		this(that.getStart(), that.getEnd());
	}
	
	public Arc(Nod start, Nod end) {
		this.start = start;
		this.end = end;
		this.flux = FLUX_INITIAL;
		this.capacitate = CAPACITATE;
	}
	
	public void calculeazaCapacitateReziduala(int fluxInvers) {
		this.capacitateReziduala = capacitate - flux + fluxInvers;
	}
}
