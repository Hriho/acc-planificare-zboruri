package main.java.planificarezboruri.graf;

import java.util.HashMap;
import java.util.Map;

import main.java.planificarezboruri.beans.InformatiiZboruri;

public class Graf {
	private Map<String, Nod> noduri;
	private Map<String, Arc> arce;

	public Map<String, Nod> getNoduri() {
		return noduri;
	}

	public void setNoduri(Map<String, Nod> noduri) {
		this.noduri = noduri;
	}

	public Map<String, Arc> getArce() {
		return arce;
	}

	public void setArce(Map<String, Arc> arce) {
		this.arce = arce;
	}

	public Graf() {
		
	}
	
	public Graf(InformatiiZboruri informatiiZboruri) {
		noduri = new HashMap<>();
		arce = new HashMap<String, Arc>();
		genereazaNoduri(informatiiZboruri.getNumarZboruri(), informatiiZboruri.getNumarEchipaje());
		genereazaArce(informatiiZboruri);
	}

	private void genereazaNoduri(int numarZboruri, int numarEchipaje) {
		int nodeValue = 0, nodeNumber = 0;
		Nod currentNode = new Nod();

		currentNode.setEticheta(Eticheta.START);
		currentNode.setValoare(nodeValue++);
		currentNode.setNumber(nodeNumber++);
		noduri.put(NODE_KEY(currentNode), new Nod(currentNode));
		currentNode.setEticheta(Eticheta.AERONAVA);

		for (int index = 0; index < numarZboruri; index++) {
			currentNode.setValoare(nodeValue++);
			currentNode.setNumber(nodeNumber++);
			noduri.put(NODE_KEY(currentNode), new Nod(currentNode));
		}
		currentNode.setEticheta(Eticheta.ECHIPAJ);
		nodeValue = 1;
		for (int index = 0; index < numarEchipaje; index++) {
			currentNode.setValoare(nodeValue++);
			currentNode.setNumber(nodeNumber++);
			noduri.put(NODE_KEY(currentNode), new Nod(currentNode));
		}

		currentNode.setEticheta(Eticheta.STOC);
		currentNode.setValoare(nodeValue++);
		currentNode.setNumber(nodeNumber++);
		noduri.put(NODE_KEY(currentNode), new Nod(currentNode));
	}

	private void genereazaArce(InformatiiZboruri informatii) {
		Arc currentArc = new Arc();
		Nod start, end;
		start = noduri.get(NODE_KEY(Eticheta.START, 0));
		currentArc.setStart(start);
		for (int index = 1; index <= informatii.getNumarZboruri(); index++) {
			end = noduri.get(NODE_KEY(Eticheta.AERONAVA, index));
			currentArc.setEnd(end);
			arce.put(ARC_KEY(start, end), new Arc(currentArc));
			start.adaugaInListaDeAdiacenta(end);
		}
		for (int index1 = 0; index1 < informatii.getNumarZboruri(); index1++) {
			for (int index2 = 0; index2 < informatii.getNumarEchipaje(); index2++) {
				if (informatii.getPlanificare()[index1][index2]) {
					start = noduri.get(NODE_KEY(Eticheta.AERONAVA, index1 + 1));
					end = noduri.get(NODE_KEY(Eticheta.ECHIPAJ, index2 + 1));
					currentArc.setStart(start);
					currentArc.setEnd(end);
					arce.put(ARC_KEY(start, end), new Arc(currentArc));
					start.adaugaInListaDeAdiacenta(end);
				}
			}
		}
		end = noduri.get(NODE_KEY(Eticheta.STOC, informatii.getNumarEchipaje() + 1));
		currentArc.setEnd(end);
		for (int index = 1; index <= informatii.getNumarEchipaje(); index++) {
			start = noduri.get(NODE_KEY(Eticheta.ECHIPAJ, index));
			currentArc.setStart(start);
			arce.put(ARC_KEY(start, end), new Arc(currentArc));
			start.adaugaInListaDeAdiacenta(end);
		}
	}
	
	public static String NODE_KEY(Nod nod) {
		return nod.getEticheta().toString().concat(Integer.toString(nod.getValoare()));
	}

	public static String NODE_KEY(Eticheta eticheta, Integer valoare) {
		return eticheta.toString().concat(Integer.toString(valoare));
	}

	public static String ARC_KEY(Nod nod1, Nod nod2) {
		return Integer.toString(nod1.getNumber()).concat(Integer.toString(nod2.getNumber()));
	}
}
