package main.java.planificarezboruri.algoritmi;

import java.util.List;

import main.java.planificarezboruri.beans.InformatiiZboruri;
import main.java.planificarezboruri.graf.Arc;
import main.java.planificarezboruri.graf.Graf;

public class FluxMaxim {

	private static Graf grafRezidual(Graf graf) {
		for (String key : graf.getArce().keySet()) {
			String cheieFluxInvers = new StringBuilder(key).reverse().toString();
			graf.getArce().get(key).calculeazaCapacitateReziduala(
					graf.getArce().containsKey(cheieFluxInvers) ? graf.getArce().get(cheieFluxInvers).getFlux() : 0);
		}
		return graf;
	}

	private static Graf updateGrafRezidual(Graf grafRezidual, List<String> keys) {
		Arc arcInvers;
		for (String key : keys) {
			arcInvers = grafRezidual.getArce().get(key).invers();
			grafRezidual.getArce().remove(key);
			grafRezidual.getArce().put(new StringBuilder(key).reverse().toString(), arcInvers);

			for (String nodeKey : grafRezidual.getNoduri().keySet()) {
				if (grafRezidual.getNoduri().get(nodeKey).getNumber().toString().equals(String.valueOf(key.charAt(0)))) {
					grafRezidual.getNoduri().get(nodeKey).removeNodeFromAdiacentList(String.valueOf(key.charAt(1)));
				}
			}
		}

		return grafRezidual;
	}

	private static Graf reverseGraf(Graf grafRezidual, Graf graf) {
		for(String key: graf.getArce().keySet()) {
			String reverseKey = new StringBuilder(key).reverse().toString();
			if(grafRezidual.getArce().containsKey(reverseKey)) {
				graf.getArce().get(key).setFlux(grafRezidual.getArce().get(reverseKey).getCapacitateReziduala());
			}
		}
		return graf;
	}

	public static Graf calculate(Graf graf, InformatiiZboruri informatii) {
		Graf grafRezidual = grafRezidual(new Graf(informatii));
		List<String> DMF = ParcurgereLatime.findDMF(grafRezidual);
		do {
			grafRezidual = updateGrafRezidual(grafRezidual, DMF);
			DMF = ParcurgereLatime.findDMF(grafRezidual);
		} while (DMF.size() != 0);
		return reverseGraf(grafRezidual, graf);
	}
}
