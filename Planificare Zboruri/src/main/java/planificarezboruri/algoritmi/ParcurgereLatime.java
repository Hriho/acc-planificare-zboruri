package main.java.planificarezboruri.algoritmi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import main.java.planificarezboruri.graf.Eticheta;
import main.java.planificarezboruri.graf.Graf;
import main.java.planificarezboruri.graf.Nod;

public class ParcurgereLatime {

	public static List<String> findDMF(Graf graf) {
		boolean foundDMF = false;
		List<String> DMF = new ArrayList<>();
		Queue<Nod> queue = new LinkedList<>();
		Nod currentNod = graf.getNoduri().get(Graf.NODE_KEY(Eticheta.START, 0));
		int[] predecesori = new int[graf.getNoduri().size()];
		boolean[] visited = new boolean[graf.getNoduri().size()];
		predecesori[currentNod.getNumber()] = -1;
		visited[currentNod.getNumber()] = true;
		queue.add(currentNod);

		while (!queue.isEmpty() && !foundDMF) {
			currentNod = queue.poll();
			visited[currentNod.getNumber()] = true;
			for (Nod nod : currentNod.getListaAdiacenta()) {
				if (!visited[nod.getNumber()]) {
					predecesori[nod.getNumber()] = currentNod.getNumber();
					queue.add(nod);
					if (nod.getEticheta().equals(Eticheta.STOC)) {
						foundDMF = true;
					}
				}
			}
		}

		if (foundDMF) {
			int index = graf.getNoduri().size() - 1;
			while (index != -1) {
				DMF.add(Integer.toString(predecesori[index]).concat(Integer.toString(index)));
				index = predecesori[index];
			}
			Collections.reverse(DMF);
			DMF.remove(0);
		}
		return DMF;
	}

}
