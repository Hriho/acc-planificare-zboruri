package main.java.planificarezboruri;

import main.java.planificarezboruri.algoritmi.FluxMaxim;
import main.java.planificarezboruri.beans.InformatiiZboruri;
import main.java.planificarezboruri.datahandler.DataHandler;
import main.java.planificarezboruri.graf.Graf;

public class PlanificareZboruri {

	public static void main(String[] args) {
		InformatiiZboruri informatii = DataHandler.read();
		DataHandler.print(FluxMaxim.calculate(new Graf(informatii), informatii));
	}
}
