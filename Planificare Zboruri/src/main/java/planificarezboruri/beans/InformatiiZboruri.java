package main.java.planificarezboruri.beans;

import java.util.Arrays;

public class InformatiiZboruri {
	private int numarZboruri;
	private int numarEchipaje;
	private boolean[][] planificare;
	
	public int getNumarZboruri() {
		return numarZboruri;
	}
	public void setNumarZboruri(int numarZboruri) {
		this.numarZboruri = numarZboruri;
	}
	public int getNumarEchipaje() {
		return numarEchipaje;
	}
	public void setNumarEchipaje(int numarEchipaje) {
		this.numarEchipaje = numarEchipaje;
	}
	public boolean[][] getPlanificare() {
		return planificare;
	}
	public void setPlanificare(boolean[][] planificare) {
		this.planificare = planificare;
	}

	public InformatiiZboruri(int numarZboruri, int numarEchipaje, boolean[][] planificare) {
		this.numarZboruri = numarZboruri;
		this.numarEchipaje = numarEchipaje;
		this.planificare = planificare;
	}
	
	@Override
	public String toString() {
		return "InformatiiZboruri [numarZboruri=" + numarZboruri + ", numarEchipaje=" + numarEchipaje + ", planificare="
				+ Arrays.toString(planificare) + "]";
	}
}
